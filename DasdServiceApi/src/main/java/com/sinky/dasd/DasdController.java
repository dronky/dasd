package com.sinky.dasd;

import com.sinky.dasd.service.DasdService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ComponentScan(basePackageClasses = DasdService.class)
public class DasdController {
}
