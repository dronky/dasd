package com.sinky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackageClasses = DasdController.class)
public class DasdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DasdApplication.class, args);
	}

}
